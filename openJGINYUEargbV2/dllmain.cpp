﻿#include "framework.h"

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
                     )
{
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH:
    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    case DLL_PROCESS_DETACH:
        break;
    }
    return TRUE;
}

int GPIOdrive(unsigned int* GRB_ptr, unsigned int num_LED,unsigned int motherboard_type)
{
    int ret = 0;
    switch (motherboard_type)
    {
    case PCH_type_ADL:
        ret = GPIOdrive_700series(GRB_ptr, num_LED);
        break;
    }
    return ret;
}


int GPIOdrive_700series(unsigned int* GRB_ptr, unsigned int num_LED)
{
    HMODULE hModule = NULL;
    hModule = LoadLibraryA("inpoutx64.dll");
    ProcMapPhysToLin MapPhysToLin = NULL;
    ProcUnmapPhysicalMemory UnmapPhysicalMemory = NULL;
    HANDLE PhysMemHANDLE = NULL;
    PDWORD pdwlinaddr = NULL;


    if (hModule == NULL)
    {
        return 0;
    }

    MapPhysToLin = (ProcMapPhysToLin)GetProcAddress(hModule, "MapPhysToLin");
    UnmapPhysicalMemory = (ProcUnmapPhysicalMemory)GetProcAddress(hModule, "UnmapPhysicalMemory");
    pdwlinaddr = (PDWORD)MapPhysToLin((PBYTE)(ALDERLAKE_GPP_DW0), 4, &PhysMemHANDLE);
    if (pdwlinaddr == NULL)
    {
        return 0;
    }
    int64_t delay_time_preset_1_ns = 10000;
    int64_t delay_time_preset_0_ns = 280000;
    int64_t delay_time_0H_ns = 300;
    int64_t delay_time_1H_ns = 2200;
    int64_t delay_time_0L_ns = 2200;
    int64_t delay_time_1L_ns = 4200;
    unsigned int package_num = num_LED * 48;

    TSCNS tscns;
    tscns.init();
    int64_t time_fin = 0;
    int64_t time0 = tscns.rdns();
    int64_t time_now = 0;
    int64_t time_next = 0;


    for (unsigned int i = 0, j = 0, k = 0, m = 0, n = 0; i <= package_num; )
    {
        time_now = tscns.rdns();

        if (m == 0)
        {
            time0 = tscns.rdns();
            m = 1;
            _mm_mfence();
            *(volatile DWORD*)pdwlinaddr = (DWORD)ALDERLAKE_GPP_DW0_LOW;
            _mm_mfence();
            time_next = time_now + delay_time_preset_0_ns;
        }
        if (i == package_num)
        {
            _mm_mfence();
            *(volatile DWORD*)pdwlinaddr = (DWORD)ALDERLAKE_GPP_DW0_HI;
            _mm_mfence();
            break;
        }
        if (time_next <= time_now)
        {
            n = i / 48;
            j = 23 - (i / 2) % 24;
            k = (GRB_ptr[n] >> j) & 0x1U;
            switch (i % 2)
            {
            case 0:
                _mm_mfence();
                *(volatile DWORD*)pdwlinaddr = (DWORD)ALDERLAKE_GPP_DW0_HI;
                _mm_mfence();
                time_fin = tscns.rdns();

                if (k && (i >= 2) )
                {
                    time_next = time_fin + delay_time_1H_ns;
                }
                else if (k && (i < 2))
                {
                    time_next = time_fin + delay_time_preset_1_ns;
                }
                else if ((!k) && (i >= 2))
                {
                    time_next = time_fin + delay_time_0H_ns;
                }
                else
                {
                    time_next = time_fin + 10;
                }

                break;
            case 1:
                _mm_mfence();
                *(volatile DWORD*)pdwlinaddr = (DWORD)ALDERLAKE_GPP_DW0_LOW;
                _mm_mfence();
                time_fin = tscns.rdns();
                if (k && (i >= 2))
                {
                    time_next = time_fin + delay_time_1L_ns;
                }
                else if(!k && (i >= 2))
                {
                    time_next = time_fin + delay_time_0L_ns;
                }
                else
                {
                    time_next = time_fin + delay_time_preset_1_ns;
                }
                break;
            default:

                break;
            }
            i++;
            if (i == package_num)
            {
                time_next = time_fin + delay_time_preset_0_ns;
            }
        }
    }
    UnmapPhysicalMemory(PhysMemHANDLE, (PBYTE)pdwlinaddr);
    FreeLibrary(hModule);
    return 1;
}

