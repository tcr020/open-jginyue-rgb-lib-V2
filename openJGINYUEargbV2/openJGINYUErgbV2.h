#pragma once
#include <Windows.h>
#define ALDERLAKE_GPP_DW0		0xE06A0A50
#define ALDERLAKE_GPP_DW0_HI	0x84000201
#define ALDERLAKE_GPP_DW0_LOW	0x84000200

#define PCH_type_ADL			1


typedef PBYTE(__stdcall* ProcMapPhysToLin)(PBYTE pbPhysAddr, DWORD dwPhysSize, HANDLE* pPhysicalMemoryHandle);
typedef BOOL(__stdcall* ProcUnmapPhysicalMemory)(HANDLE PhysicalMemoryHandle, PBYTE pbLinAddr);
typedef unsigned short(__stdcall* ProcDlPortReadPortUshort)(unsigned short port);
typedef           void(__stdcall* ProcDlPortWritePortUshort)(unsigned short port, unsigned short value);


int GPIOdrive(unsigned int* GRB_ptr, unsigned int num_LED, unsigned int motherboard_type);
int GPIOdrive_700series(unsigned int* GRB_ptr, unsigned int num_LED);

